FROM mcr.microsoft.com/java/jre:15-zulu-alpine

RUN mkdir /app

COPY ./build/libs/professors-ms-0.0.1-SNAPSHOT.jar /app/app.jar

ENTRYPOINT ["java", "-jar","/app/app.jar"]
