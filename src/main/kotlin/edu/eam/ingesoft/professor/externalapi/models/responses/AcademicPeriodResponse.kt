package edu.eam.ingesoft.professor.externalapi.models.responses

data class AcademicPeriodResponse(

    val id: String,

    val startDate: String,

    val endDate: String
)
