package edu.eam.ingesoft.professor.externalapi.services

import edu.eam.ingesoft.professor.externalapi.client.AcademicMsClient
import edu.eam.ingesoft.professor.externalapi.models.responses.AcademicPeriodResponse
import edu.eam.ingesoft.professor.externalapi.models.responses.AcademicProgramResponse
import edu.eam.ingesoft.professor.externalapi.models.responses.ProgramResponse
import feign.FeignException
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Component
import javax.persistence.EntityNotFoundException

@Component
class AcademicMSService {

    @Autowired
    lateinit var academicMsClient: AcademicMsClient

    fun findProgram(idProgram: Long): ProgramResponse {
        try {
            return academicMsClient.getProgram(idProgram)
        } catch (exc: FeignException.NotFound) {
            throw EntityNotFoundException("The program doesn't exist")
        } catch (exc: FeignException) {
            throw Exception("Unexpected Exception")
        }
    }

    fun listProgramsByFaculty(id: Long): List<AcademicProgramResponse> {
        try {
            return academicMsClient.listProgramsByFaculty(id)
        } catch (exc: FeignException.NotFound) {
            throw EntityNotFoundException("Faculty Not Found")
        } catch (exc: FeignException) {
            throw Exception("Unexpected Exception")
        }
    }

    fun findActivity(id: String) {
        try {
            academicMsClient.findActivity(id)
        } catch (exc: FeignException.NotFound) {
            throw EntityNotFoundException("Activity Not Found")
        } catch (exc: FeignException) {
            throw Exception("Unexpected Exception")
        }
    }

    fun getActivity(id: Long) {
        try {
            academicMsClient.getActivity(id)
        } catch (exc: FeignException.NotFound) {
            throw EntityNotFoundException("Activity Not Found")
        } catch (exc: FeignException) {
            throw Exception("Unexpected Exception")
        }
    }

    fun getAcademicPeriodIdByCurrentDate(): AcademicPeriodResponse {
        try {
            return academicMsClient.getCurrentPeriod()
        } catch (exc: FeignException.NotFound) {
            throw EntityNotFoundException("Academic period was not founded")
        } catch (exc: FeignException) {
            throw Exception("Unexpected Exception")
        }
    }
}
