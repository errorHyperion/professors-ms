package edu.eam.ingesoft.professor.externalapi.models.responses

data class ActivityResponse(
    val id: Long,
    val name: String,
    val description: String
)
