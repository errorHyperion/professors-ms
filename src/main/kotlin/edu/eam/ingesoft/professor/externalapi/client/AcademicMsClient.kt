package edu.eam.ingesoft.professor.externalapi.client

import edu.eam.ingesoft.professor.externalapi.models.responses.AcademicPeriodResponse
import edu.eam.ingesoft.professor.externalapi.models.responses.AcademicProgramResponse
import edu.eam.ingesoft.professor.externalapi.models.responses.ActivityResponse
import edu.eam.ingesoft.professor.externalapi.models.responses.ProgramResponse
import org.springframework.cloud.openfeign.FeignClient
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestMethod

@FeignClient(name = "AcademicMSClient", url = "\${externalapi.academic-ms}/api/academic-ms")
interface AcademicMsClient {

    @RequestMapping(method = arrayOf(RequestMethod.GET), path = arrayOf("/faculties/{idFaculty}/programs"))
    fun listProgramsByFaculty(@PathVariable idFaculty: Long): List<AcademicProgramResponse>

    @RequestMapping(method = arrayOf(RequestMethod.GET), path = arrayOf("/activities/{idActivity}"))
    fun findActivity(@PathVariable idActivity: String): ActivityResponse

    @RequestMapping(method = arrayOf(RequestMethod.GET), path = arrayOf("/programs/{idProgram}"))
    fun getProgram(@PathVariable("idProgram") idProgram: Long): ProgramResponse

    @RequestMapping(method = arrayOf(RequestMethod.GET), path = arrayOf("/{id}/find"))
    fun getActivity(@PathVariable id: Long): ActivityResponse

    @RequestMapping(method = arrayOf(RequestMethod.GET), path = arrayOf("/academic-periods/current"))
    fun getCurrentPeriod(): AcademicPeriodResponse
}
