package edu.eam.ingesoft.professor.externalapi.models.responses

data class FacultyResponse(
    val id: Long,
    val name: String,
    val status: Boolean
)
