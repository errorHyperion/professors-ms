package edu.eam.ingesoft.professor.security.authclient.model

data class SecurityPayload(
    val username: String,
    val groups: List<String>,
    val permissions: List<String>,
)
