package edu.eam.ingesoft.professor.security.authclient

import edu.eam.ingesoft.professor.security.authclient.model.SecurityPayload
import edu.eam.ingesoft.professor.security.authclient.model.Token
import feign.FeignException
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Component

@Component
class SecurityService {

    @Autowired
    lateinit var securityClient: SecurityClient

    fun validateToken(token: String): SecurityPayload {
        try {
            return securityClient.validateToken(Token(token))
        } catch (exc: FeignException.Forbidden) {
            exc.printStackTrace()
            throw SecurityException("Invalid Token")
        } catch (exc: FeignException) {
            exc.printStackTrace()
            throw Exception("Unexpected Exception")
        }
    }
}
