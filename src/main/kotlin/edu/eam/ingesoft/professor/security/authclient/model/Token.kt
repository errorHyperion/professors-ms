package edu.eam.ingesoft.professor.security.authclient.model

data class Token(
    val token: String
)
