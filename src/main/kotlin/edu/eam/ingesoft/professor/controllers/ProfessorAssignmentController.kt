package edu.eam.ingesoft.professor.controllers

import edu.eam.ingesoft.professor.config.Groups
import edu.eam.ingesoft.professor.config.Permissions
import edu.eam.ingesoft.professor.config.Routes
import edu.eam.ingesoft.professor.model.requests.CreateAssignmentRequest
import edu.eam.ingesoft.professor.model.requests.EditAssignmentRequest
import edu.eam.ingesoft.professor.security.Secured
import edu.eam.ingesoft.professor.services.ProfessorAssignmentService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.data.domain.Pageable
import org.springframework.data.web.PageableDefault
import org.springframework.web.bind.annotation.DeleteMapping
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.PutMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping(Routes.PROFESSOR_PATH)
class ProfessorAssignmentController {

    @Autowired
    lateinit var professorAssignmentService: ProfessorAssignmentService

    @Secured(permissions = [Permissions.ADD_TEACHER_ASSIGNMENT], groups = [Groups.PROGRAM_DIRECTOR, Groups.CLASS_TEACHER, Groups.SYSTEM_ADMINISTRATOR])
    @PostMapping(Routes.CREATE_ASSIGNMENT_BY_PROFESSOR)
    fun create(@RequestBody request: CreateAssignmentRequest, @PathVariable idProfessor: String) = professorAssignmentService.create(idProfessor, request.typeId, request.quantity, request.assignmentId)

    @Secured(
        permissions = [Permissions.FIND_PROFESSOR_ASSIGNMENT],
        groups = [
            Groups.CLASS_TEACHER,
            Groups.PROGRAM_DIRECTOR,
            Groups.SYSTEM_ADMINISTRATOR
        ]
    )
    @GetMapping(Routes.FIND_ASSIGNMENT_BY_PROFESSOR)
    fun find(@PathVariable idProfessor: String) = professorAssignmentService.find(idProfessor)

    @Secured(
        permissions = [Permissions.EDIT_PROFESSOR_ASSIGNMENT],
        groups = [Groups.CLASS_TEACHER, Groups.PROGRAM_DIRECTOR, Groups.SYSTEM_ADMINISTRATOR]
    )
    @PutMapping(Routes.EDIT_ASSIGNMENT_BY_PROFESSOR)
    fun edit(@PathVariable idProfessor: String, @PathVariable typeId: Long, @RequestBody request: EditAssignmentRequest) = professorAssignmentService.edit(typeId, idProfessor, request.quantity)

    @Secured(permissions = [Permissions.DELETE_PROFESSORS_ASSIGNMENT], groups = [Groups.CLASS_TEACHER, Groups.PROGRAM_DIRECTOR])
    @DeleteMapping(Routes.DELETE_ASSIGNMENT_BY_PROFESSOR)
    fun delete(@PathVariable typeId: Long, @PathVariable idProfessor: String) = professorAssignmentService.delete(idProfessor, typeId)

    @Secured(
        permissions = [Permissions.FIND_PROFESSOR_ASSIGNMENT_BY_PROGRAM_ID],
        groups = [Groups.CLASS_TEACHER, Groups.PROGRAM_DIRECTOR, Groups.SYSTEM_ADMINISTRATOR]
    )
    @GetMapping(Routes.PROFESSOR_ASSIGNMENT_BY_PROGRAM)
    fun findAssignmentByProgram(@PathVariable idProgram: Long, @PageableDefault(size = 5) pageable: Pageable) = professorAssignmentService.listAllByIdProgram(idProgram, pageable)
}
