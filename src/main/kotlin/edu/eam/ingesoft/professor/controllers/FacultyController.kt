package edu.eam.ingesoft.professor.controllers

import edu.eam.ingesoft.professor.config.Routes
import edu.eam.ingesoft.professor.services.ProfessorService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.data.domain.Pageable
import org.springframework.data.web.PageableDefault
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping(Routes.FACULTY_PATH)
class FacultyController {

    @Autowired
    lateinit var professorService: ProfessorService

    @GetMapping(Routes.LIST_PROFESSORS_BY_FACULTY_PATH)
    fun listProfessorsByFaculty(@PathVariable("idFaculty") idFaculty: Long, @PageableDefault(size = 5) pageable: Pageable) = professorService.listProfessorsByFaculty(idFaculty, pageable)
}
