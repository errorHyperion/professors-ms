package edu.eam.ingesoft.professor.repositories

import edu.eam.ingesoft.professor.model.entities.Professor
import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
import org.springframework.data.jpa.repository.Query
import org.springframework.data.repository.PagingAndSortingRepository
import org.springframework.stereotype.Repository

@Repository
interface ProfessorRepository : PagingAndSortingRepository<Professor, String> {

    @Query("SELECT p FROM Professor p WHERE p.idProgram =:idProgram")
    fun findByIdProgram(idProgram: Long, pageable: Pageable?): Page<Professor>

    fun findByIdentificationNumber(identificationNumber: String): Professor?

    fun existsByIdentificationNumber(identificationNumber: String): Boolean

    @Query("SELECT p FROM Professor p WHERE p.idProgram IN :academicPrograms")
    fun listProfessorByFaculty(academicPrograms: List<Long>, pageable: Pageable): Page<Professor>
}
