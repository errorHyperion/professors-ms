package edu.eam.ingesoft.professor.repositories

import edu.eam.ingesoft.professor.model.entities.Diary
import org.springframework.data.jpa.repository.Query
import org.springframework.data.repository.CrudRepository
import org.springframework.stereotype.Repository

@Repository
interface DiaryRepository : CrudRepository<Diary, Long> {
    @Query("SELECT dia FROM Diary dia WHERE  dia.id =:diaryId AND dia.professor.code =:professorCode")
    fun findByCodeProfessorAndDiaryId(diaryId: Long, professorCode: String): Diary?
}
