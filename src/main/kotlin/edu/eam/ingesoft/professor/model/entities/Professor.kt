package edu.eam.ingesoft.professor.model.entities

import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.Id
import javax.persistence.Table

@Entity
@Table(name = "professor")
data class Professor(
    @Id
    val code: String,

    val identificationNumber: String,

    val name: String,

    @Column(name = "last_name")
    val lastName: String,

    val email: String,

    @Column(name = "phone_number")
    val phoneNumber: String,

    val biding: String,

    @Column(name = "id_program")
    val idProgram: Long,

    val scale: String,

    var enabled: Boolean? = true,
)
