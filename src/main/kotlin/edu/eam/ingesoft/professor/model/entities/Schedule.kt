package edu.eam.ingesoft.professor.model.entities

import com.fasterxml.jackson.annotation.JsonBackReference
import java.time.DayOfWeek
import java.util.Date
import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.EnumType
import javax.persistence.Enumerated
import javax.persistence.GeneratedValue
import javax.persistence.GenerationType
import javax.persistence.Id
import javax.persistence.JoinColumn
import javax.persistence.ManyToOne
import javax.persistence.Table
import javax.validation.constraints.NotNull

@Entity
@Table(name = "schedules")
data class Schedule(
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    val id: Long? = 0L,

    @Enumerated(EnumType.STRING)
    val day: DayOfWeek,

    @Column(name = "starts_at")
    val startAt: Date,

    @Column(name = "ends_at")
    val endsAt: Date,

    @Column(name = "type_id")
    val typeId: String,

    @ManyToOne
    @JoinColumn(name = "id_diary", referencedColumnName = "id")
    @JsonBackReference
    @NotNull
    val diary: Diary
)
