package edu.eam.ingesoft.professor.model.requests

import java.time.DayOfWeek
import java.util.Date

data class CreateScheduleRequest(
    val day: DayOfWeek,
    val startAt: Date,
    val endsAt: Date,
    val typeId: String
)
