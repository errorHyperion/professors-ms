package edu.eam.ingesoft.professor.model.requests

data class EditAssignmentRequest(
    val typeId: Long,
    val quantity: Int
)
