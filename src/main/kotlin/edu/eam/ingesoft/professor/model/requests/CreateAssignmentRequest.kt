package edu.eam.ingesoft.professor.model.requests

data class CreateAssignmentRequest(
    val typeId: Long,
    val quantity: Int,
    val assignmentId: Long
)
