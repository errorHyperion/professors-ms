package edu.eam.ingesoft.professor.model.responses

data class ErrorResponse(
    val status: Int? = 500,
    val message: String? = "Exception"
)
