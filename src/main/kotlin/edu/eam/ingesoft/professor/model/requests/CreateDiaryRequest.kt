package edu.eam.ingesoft.professor.model.requests

data class CreateDiaryRequest(
    val academicPeriod: String
)
