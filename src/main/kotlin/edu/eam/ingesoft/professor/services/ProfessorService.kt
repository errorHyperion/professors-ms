package edu.eam.ingesoft.professor.services

import edu.eam.ingesoft.professor.exceptions.BusinessException
import edu.eam.ingesoft.professor.externalapi.services.AcademicMSService
import edu.eam.ingesoft.professor.model.entities.Professor
import edu.eam.ingesoft.professor.repositories.ProfessorRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
import org.springframework.http.HttpStatus
import org.springframework.stereotype.Service
import javax.persistence.EntityNotFoundException

@Service
class ProfessorService {
    @Autowired
    lateinit var professorRepository: ProfessorRepository

    @Autowired
    lateinit var academicMSService: AcademicMSService

    fun find(code: String): Professor? =
        professorRepository.findById(code).orElseThrow { EntityNotFoundException("Professor Not Found") }

    fun create(professor: Professor) {

        academicMSService.findProgram(professor.idProgram)

        if (professorRepository.existsById(professor.code)) throw BusinessException(
            "Professor already created with that code",
            HttpStatus.PRECONDITION_FAILED
        )

        if (professorRepository.existsByIdentificationNumber(professor.identificationNumber)) throw BusinessException(
            "Professor already created with that identification number",
            HttpStatus.PRECONDITION_FAILED
        )

        professorRepository.save(professor)
    }

    fun edit(professor: Professor, code: String) {

        if (!professorRepository.existsById(code)) throw BusinessException(
            "Professor doesn't exist",
            HttpStatus.NOT_FOUND
        )

        val professorFound = professorRepository.findByIdentificationNumber(professor.identificationNumber)

        if (professorFound != null && professorFound.code != code) throw BusinessException(
            "Professor already created with that identification number",
            HttpStatus.PRECONDITION_FAILED
        )

        academicMSService.findProgram(professor.idProgram)

        professorRepository.save(professor)
    }

    fun findProfessorsByAcademicProgram(idProgram: Long, pageable: Pageable?): Page<Professor> {

        val program = academicMSService.findProgram(idProgram)

        return professorRepository.findByIdProgram(program.id, pageable)
    }

    fun activate(code: String) {
        val professor =
            professorRepository.findById(code).orElseThrow { EntityNotFoundException("Professor Not Found") }

        if (professor.enabled == true) throw BusinessException(
            "Professor already activated",
            HttpStatus.PRECONDITION_FAILED
        )

        professor.enabled = true

        professorRepository.save(professor)
    }

    fun deactivate(code: String) {
        val professor =
            professorRepository.findById(code).orElseThrow { EntityNotFoundException("Professor Not Found") }

        if (professor.enabled == false) throw BusinessException(
            "Professor already deactivated",
            HttpStatus.PRECONDITION_FAILED
        )

        professor.enabled = false

        professorRepository.save(professor)
    }

    fun listProfessorsByFaculty(facultyId: Long, pageable: Pageable): Page<Professor> {
        val programs = academicMSService.listProgramsByFaculty(facultyId).map { it.id }

        return professorRepository.listProfessorByFaculty(programs, pageable)
    }
}
