package edu.eam.ingesoft.professor.services

import edu.eam.ingesoft.professor.exceptions.BusinessException
import edu.eam.ingesoft.professor.externalapi.services.AcademicMSService
import edu.eam.ingesoft.professor.model.entities.Diary
import edu.eam.ingesoft.professor.repositories.DiaryRepository
import edu.eam.ingesoft.professor.repositories.ProfessorRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.stereotype.Service

@Service
class DiaryService {

    @Autowired
    lateinit var diaryRepository: DiaryRepository

    @Autowired
    lateinit var academicMSService: AcademicMSService

    @Autowired
    lateinit var professorRepository: ProfessorRepository

    fun createDiary(professorCode: String) {

        val academicPeriodId = academicMSService.getAcademicPeriodIdByCurrentDate().id

        val professor = professorRepository.findById(professorCode)

        if (professor.isEmpty) throw BusinessException(
            "The professor is not founded",
            HttpStatus.NOT_FOUND
        )

        val academicPeriod = academicMSService.getAcademicPeriodIdByCurrentDate().id
        val diary: Diary = Diary(professor = professor.get(), academicPeriod = academicPeriod)
        diaryRepository.save(diary)
    }

    fun findTheTeachersDiary(professorCode: String, diaryId: Long): Diary {

        if (!professorRepository.existsById(professorCode)) throw BusinessException(
            "The professor is not founded",
            HttpStatus.NOT_FOUND
        )

        if (!diaryRepository.existsById(diaryId)) throw BusinessException(
            "The diary is not founded",
            HttpStatus.NOT_FOUND
        )
        return diaryRepository.findByCodeProfessorAndDiaryId(diaryId, professorCode)
            ?: throw BusinessException("The teachers diary is not assigned", HttpStatus.FORBIDDEN)
    }
}
