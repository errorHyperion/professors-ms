package edu.eam.ingesoft.professor.config

object Permissions {
    const val LIST_OF_TEACHERS_BY_ACADEMIC_PROGRAM = "listOfTeachersByAcademicProgram"
    const val CREATE_PROFESSOR = "createProfessor"
    const val ACTIVATE_PROFESSOR = "activateProfessor"
    const val FIND_PROFESSOR_ASSIGNMENT = "findProfessorAssignment"
    const val EDIT_SCHEDULE_PROFESSOR = "editScheduleProfessor"
    const val CREATE_DIARY_PROFESSOR = "createDiaryProfessor"
    const val DEACTIVATE_PROFESSOR = "deactivateProfessor"
    const val FIND_PROFESSOR_ASSIGNMENT_BY_PROGRAM_ID = "findProfessorAssignmentByProgram"
    const val EDIT_PROFESSOR_ASSIGNMENT = "editProfessorAssignment"
    const val DELETE_SCHEDULE_PROFESSOR = "deleteScheduleProfessor"
    const val EDIT_PROFESSOR = "editProfessor"
    const val ADD_TEACHER_ASSIGNMENT = "addTeacherAssignment"
    const val DELETE_PROFESSORS_ASSIGNMENT = "deleteProfessorAssignment"
    const val CREATE_SCHEDULE_PROFESSOR = "createScheduleProfessor"
    const val FIND_PROFESSOR = "findProfessor"
    const val FIND_SCHEDULE_PROFESSOR = "findScheduleProfessor"
}
