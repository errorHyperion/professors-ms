package edu.eam.ingesoft.professor.config

object Groups {
    const val PROGRAM_DIRECTOR = "programDirector"
    const val CLASS_TEACHER = "classTeacher"
    const val SYSTEM_ADMINISTRATOR = "systemAdministrator"
}
