package edu.eam.ingesoft.professor.config

object Routes {
    const val PATH = "/path"
    const val PROGRAM_PATH = "/programs"
    const val FACULTY_PATH = "/faculties"
    const val PROFESSOR_PATH = "/professors"
    const val ACTIVITY_PATH = "/activities"

    const val ADD_DIARY_BY_CODE_PROFESSOR = "/{professorCode}/diaries"
    const val FIND_DIARY_BY_CODE_PROFESSOR_AND_DIARY_ID = "/{professorCode}/diaries/{diaryId}"
    const val ADD_SCHEDULE_BY_CODE_PROFESSOR_AND_DIARY_ID = "/{professorCode}/diaries/{diaryId}/schedules"
    const val EDIT_SCHEDULE_BY_CODE_PROFESSOR_AND_SCHEDULE_ID = "/{professorCode}/diaries/schedules/{scheduleId}"
    const val DELETE_SCHEDULE_BY_SCHEDULE_ID = "/schedules/{scheduleId}"
    const val LIST_PROFESSOR_BY_ACADEMIC_PROGRAM_PATH = "/{idProgram}/professors"
    const val EDIT_PROFESSOR_PATH = "/{code}"
    const val CREATE_ASSIGNMENT_BY_PROFESSOR = "/{idProfessor}/assignments"
    const val FIND_ASSIGNMENT_BY_PROFESSOR = "/find/{idProfessor}/assignments"
    const val EDIT_ASSIGNMENT_BY_PROFESSOR = "/{idProfessor}/assignments/{typeId}"
    const val DELETE_ASSIGNMENT_BY_PROFESSOR = "/delete/{idProfessor}/assignments/{typeId}"
    const val FIND_PROFESSOR_PATH = "/{code}"
    const val ACTIVATE_PROFESSOR_PATH = "/{code}/activate"
    const val DEACTIVATE_PROFESSOR_PATH = "/{code}/deactivate"
    const val LIST_PROFESSORS_BY_FACULTY_PATH = "/{idFaculty}/professors"
    const val PROFESSOR_ASSIGNMENT_BY_PROGRAM = "{idProgram}/assignments"
}
