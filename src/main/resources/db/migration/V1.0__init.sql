CREATE TABLE public.professor (
	code varchar(255) NOT NULL,
	biding varchar(255) NULL,
	email varchar(255) NULL,
	enabled bool NULL,
	id_program int8 NULL,
	identification_number varchar(255) NULL,
	last_name varchar(255) NULL,
	"name" varchar(255) NULL,
	phone_number varchar(255) NULL,
	"scale" varchar(255) NULL,
	CONSTRAINT professor_pkey PRIMARY KEY (code)
);

CREATE TABLE public.diaries (
	id SERIAL,
	academic_period varchar(255) NULL,
	code_professor varchar(255) NULL,
	CONSTRAINT diaries_pkey PRIMARY KEY (id),
	CONSTRAINT fkpg30hu4yjpa5qq1ewa1uxj10e FOREIGN KEY (code_professor) REFERENCES professor(code)
);

CREATE TABLE public.professor_assignment (
	id SERIAL,
	quantity int4 NOT NULL,
	type_id int8 NULL,
	code_professor varchar(255) NULL,
	CONSTRAINT professor_assignment_pkey PRIMARY KEY (id),
	CONSTRAINT fk901j3fmrbode6o04o0vj31ufx FOREIGN KEY (code_professor) REFERENCES professor(code)
);

CREATE TABLE public.schedules (
	id SERIAL,
	"day" varchar(255) NULL,
	ends_at timestamp NULL,
	starts_at timestamp NULL,
	type_id varchar(255) NULL,
	id_diary int8 NULL,
	CONSTRAINT schedules_pkey PRIMARY KEY (id),
	CONSTRAINT fkhxrmar46aklnqev63wkdmsvxy FOREIGN KEY (id_diary) REFERENCES diaries(id)
);