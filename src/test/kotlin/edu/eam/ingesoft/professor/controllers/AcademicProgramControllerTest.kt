package edu.eam.ingesoft.professor.controllers

import CommonTests
import edu.eam.ingesoft.professor.config.Groups
import edu.eam.ingesoft.professor.config.Permissions
import edu.eam.ingesoft.professor.config.Routes
import edu.eam.ingesoft.professor.externalapi.models.responses.ProgramResponse
import org.hamcrest.Matchers
import org.junit.jupiter.api.Test
import org.mockito.Mockito
import org.springframework.http.MediaType
import org.springframework.test.context.jdbc.Sql
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders
import org.springframework.test.web.servlet.result.MockMvcResultMatchers

class AcademicProgramControllerTest : CommonTests() {

    @Test
    @Sql("/queries/ProfessorControllerTest/findProfessorsByAcademicProgramTest.sql")
    fun findProfessorsByAcademicProgramTest() {
        val idProgram = 1L
        val program: ProgramResponse = ProgramResponse(idProgram, "ProgramResponse Mockito")

        Mockito.`when`(academicMsClient.getProgram(program.id)).thenReturn(program)

        val header = mockSecurity(
            "Token",
            groups = listOf(
                Groups.PROGRAM_DIRECTOR,
                Groups.SYSTEM_ADMINISTRATOR
            ),
            permissions = listOf(Permissions.LIST_OF_TEACHERS_BY_ACADEMIC_PROGRAM)
        )

        val request = MockMvcRequestBuilders
            .get("${Routes.PROGRAM_PATH}/${Routes.LIST_PROFESSOR_BY_ACADEMIC_PROGRAM_PATH}", program.id)
            .contentType(MediaType.APPLICATION_JSON)
            .header(header.name, header.bearer)

        val response = mockMvc.perform(request)
        response.andExpect(MockMvcResultMatchers.status().isOk)
            .andExpect(MockMvcResultMatchers.jsonPath("content", Matchers.hasSize<Int>(3)))
            .andExpect(MockMvcResultMatchers.jsonPath("content[0].id_program", Matchers.`is`(1)))
            .andExpect(MockMvcResultMatchers.jsonPath("content[1].id_program", Matchers.`is`(1)))
            .andExpect(MockMvcResultMatchers.jsonPath("content[2].id_program", Matchers.`is`(1)))
    }

    @Test
    @Sql("/queries/ProfessorControllerTest/findProfessorsAcademicProgramAcademicProgramNotExist.sql")
    fun findProfessorsByAcademicProgramWhenAcademicProgramNotExistInAcademicMsTest() {
        val idProgram = 0L
        val program: ProgramResponse = ProgramResponse(idProgram, "ProgramResponse Mockito")

        Mockito.`when`(academicMsClient.getProgram(program.id)).thenThrow(createFeignNotFoundException())

        val header = mockSecurity(
            "Token",
            groups = listOf(
                Groups.PROGRAM_DIRECTOR,
                Groups.SYSTEM_ADMINISTRATOR
            ),
            permissions = listOf(Permissions.LIST_OF_TEACHERS_BY_ACADEMIC_PROGRAM)
        )

        val request = MockMvcRequestBuilders
            .get("${Routes.PROGRAM_PATH}/${Routes.LIST_PROFESSOR_BY_ACADEMIC_PROGRAM_PATH}", program.id)
            .contentType(MediaType.APPLICATION_JSON)
            .header(header.name, header.bearer)

        val response = mockMvc.perform(request)
        response.andExpect(MockMvcResultMatchers.status().isNotFound)
            .andExpect(MockMvcResultMatchers.jsonPath("$.status", Matchers.`is`(404)))
            .andExpect(MockMvcResultMatchers.jsonPath("$.message", Matchers.`is`("The program doesn't exist")))
    }

    @Test
    fun findProfessorsByAcademicProgramEmptyTest() {
        val idProgram = 2L
        val program: ProgramResponse = ProgramResponse(idProgram, "ProgramResponse Mockito")

        Mockito.`when`(academicMsClient.getProgram(program.id)).thenReturn(program)

        val header = mockSecurity(
            "Token",
            groups = listOf(
                Groups.PROGRAM_DIRECTOR,
                Groups.SYSTEM_ADMINISTRATOR
            ),
            permissions = listOf(Permissions.LIST_OF_TEACHERS_BY_ACADEMIC_PROGRAM)
        )

        val request = MockMvcRequestBuilders
            .get("${Routes.PROGRAM_PATH}/${Routes.LIST_PROFESSOR_BY_ACADEMIC_PROGRAM_PATH}", program.id)
            .contentType(MediaType.APPLICATION_JSON)
            .header(header.name, header.bearer)

        val response = mockMvc.perform(request)
        response.andExpect(MockMvcResultMatchers.status().isOk)
            .andExpect(MockMvcResultMatchers.jsonPath("content", Matchers.hasSize<Int>(0)))
    }

    @Test
    @Sql("/queries/ProfessorControllerTest/findProfessorsAcademicProgramPaginationSize.sql")
    fun findProfessorsByAcademicProgramWithPaginationSizeTest() {
        val idProgram = 1L
        val program: ProgramResponse = ProgramResponse(idProgram, "ProgramResponse Mockito")

        Mockito.`when`(academicMsClient.getProgram(program.id)).thenReturn(program)

        val header = mockSecurity(
            "Token",
            groups = listOf(
                Groups.PROGRAM_DIRECTOR,
                Groups.SYSTEM_ADMINISTRATOR
            ),
            permissions = listOf(Permissions.LIST_OF_TEACHERS_BY_ACADEMIC_PROGRAM)
        )

        val request = MockMvcRequestBuilders
            .get("${Routes.PROGRAM_PATH}/${Routes.LIST_PROFESSOR_BY_ACADEMIC_PROGRAM_PATH}?size=4", program.id)
            .contentType(MediaType.APPLICATION_JSON)
            .header(header.name, header.bearer)

        val response = mockMvc.perform(request)
        response.andExpect(MockMvcResultMatchers.status().isOk)
            .andExpect(MockMvcResultMatchers.jsonPath("content", Matchers.hasSize<Int>(4)))
            .andExpect(MockMvcResultMatchers.jsonPath("content[0].id_program", Matchers.`is`(1)))
            .andExpect(MockMvcResultMatchers.jsonPath("content[1].id_program", Matchers.`is`(1)))
            .andExpect(MockMvcResultMatchers.jsonPath("content[2].id_program", Matchers.`is`(1)))
            .andExpect(MockMvcResultMatchers.jsonPath("content[3].id_program", Matchers.`is`(1)))
            .andExpect(MockMvcResultMatchers.jsonPath("pageable.page_size", Matchers.`is`(4)))
    }

    @Test
    @Sql("/queries/ProfessorControllerTest/findProfessorsAcademicProgramPaginationSizeAnNumbersql")
    fun findProfessorsByAcademicProgramWithPaginationSizeAndPageNumberTest() {
        val idProgram = 1L
        val program: ProgramResponse = ProgramResponse(idProgram, "ProgramResponse Mockito")

        Mockito.`when`(academicMsClient.getProgram(program.id)).thenReturn(program)

        val header = mockSecurity(
            "Token",
            groups = listOf(
                Groups.PROGRAM_DIRECTOR,
                Groups.SYSTEM_ADMINISTRATOR
            ),
            permissions = listOf(Permissions.LIST_OF_TEACHERS_BY_ACADEMIC_PROGRAM)
        )

        val request = MockMvcRequestBuilders
            .get("${Routes.PROGRAM_PATH}/${Routes.LIST_PROFESSOR_BY_ACADEMIC_PROGRAM_PATH}?size=4&page=1", program.id)
            .contentType(MediaType.APPLICATION_JSON)
            .header(header.name, header.bearer)

        val response = mockMvc.perform(request)
        response.andExpect(MockMvcResultMatchers.status().isOk)
            .andExpect(MockMvcResultMatchers.jsonPath("content", Matchers.hasSize<Int>(4)))
            .andExpect(MockMvcResultMatchers.jsonPath("content[0].id_program", Matchers.`is`(1)))
            .andExpect(MockMvcResultMatchers.jsonPath("content[1].id_program", Matchers.`is`(1)))
            .andExpect(MockMvcResultMatchers.jsonPath("content[2].id_program", Matchers.`is`(1)))
            .andExpect(MockMvcResultMatchers.jsonPath("content[3].id_program", Matchers.`is`(1)))
            .andExpect(MockMvcResultMatchers.jsonPath("pageable.page_size", Matchers.`is`(4)))
            .andExpect(MockMvcResultMatchers.jsonPath("pageable.page_number", Matchers.`is`(1)))
    }

    @Test
    @Sql("/queries/ProfessorControllerTest/findProfessorsAcademicProgramPaginationSort.sql")
    fun findProfessorsByAcademicProgramWithPaginationSizeAndPageNumberSortTest() {
        val idProgram = 1L
        val program: ProgramResponse = ProgramResponse(idProgram, "ProgramResponse Mockito")

        Mockito.`when`(academicMsClient.getProgram(program.id)).thenReturn(program)

        val header = mockSecurity(
            "Token",
            groups = listOf(
                Groups.PROGRAM_DIRECTOR,
                Groups.SYSTEM_ADMINISTRATOR
            ),
            permissions = listOf(Permissions.LIST_OF_TEACHERS_BY_ACADEMIC_PROGRAM)
        )

        val request = MockMvcRequestBuilders
            .get(
                "${Routes.PROGRAM_PATH}/${Routes.LIST_PROFESSOR_BY_ACADEMIC_PROGRAM_PATH}?size=4&page=0&sort=name",
                program.id
            )
            .contentType(MediaType.APPLICATION_JSON)
            .header(header.name, header.bearer)

        val response = mockMvc.perform(request)
        response.andExpect(MockMvcResultMatchers.status().isOk)
            .andExpect(MockMvcResultMatchers.jsonPath("content", Matchers.hasSize<Int>(4)))
            .andExpect(MockMvcResultMatchers.jsonPath("content[0].name", Matchers.`is`("Alee")))
            .andExpect(MockMvcResultMatchers.jsonPath("content[1].name", Matchers.`is`("Balker")))
            .andExpect(MockMvcResultMatchers.jsonPath("content[2].name", Matchers.`is`("Crory")))
            .andExpect(MockMvcResultMatchers.jsonPath("content[3].name", Matchers.`is`("Krory")))
            .andExpect(MockMvcResultMatchers.jsonPath("pageable.page_size", Matchers.`is`(4)))
            .andExpect(MockMvcResultMatchers.jsonPath("pageable.page_number", Matchers.`is`(0)))
            .andExpect(MockMvcResultMatchers.jsonPath("pageable.sort.sorted", Matchers.`is`(true)))
    }
}
