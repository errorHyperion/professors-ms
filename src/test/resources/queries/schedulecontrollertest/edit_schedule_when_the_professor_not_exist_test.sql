INSERT INTO professor (code, biding, email, enabled, id_program, identification_number, last_name, "name", phone_number, "scale") VALUES ('01', 'Professor', 'email1@email.com',true, 0, '1010', 'Apellido 1', 'Nombre 1', 'telefono 1', 'scale 1');

INSERT INTO diaries (id, academic_period, code_professor) VALUES(65, 'academic_1', '01');
INSERT INTO diaries (id, academic_period, code_professor) VALUES(66, 'academic_2', '01');
INSERT INTO diaries (id, academic_period, code_professor) VALUES(67, 'academic_3', '01');

INSERT INTO schedules (id, "day", ends_at, starts_at, type_id, id_diary) VALUES(21, 'MONDAY', '2021-03-08 12:00:00', '2021-03-08 10:00:00', '1', 65);
INSERT INTO schedules (id, "day", ends_at, starts_at, type_id, id_diary) VALUES(22, 'TUESDAY', '2021-03-09 12:00:00', '2021-03-09 10:00:00', '1', 65);
INSERT INTO schedules (id, "day", ends_at, starts_at, type_id, id_diary) VALUES(23, 'WEDNESDAY', '2021-03-10 12:00:00', '2021-03-09 10:00:00', '1', 65);